from matplotlib.pyplot import *

from CieXYZ import CIEXYZ
from DiaChrom import DiaChrom
from XYZ2RGB import XYZ2RGB

if __name__ == '__main__':
    adobeRGB = XYZ2RGB([0.64, 0.33], [0.21, 0.71], [0.15, 0.06], [0.3127, 0.329])
    oneplus6t = XYZ2RGB([0.648, 0.348], [0.232, 0.671], [0.143, 0.034], [0.298, 0.314])
    oneplus6tManufacturer = XYZ2RGB([0.680, 0.320], [0.265, 0.690], [0.150, 0.060], [0.314, 0.351])
    ciexyz = CIEXYZ("./ciexyz31.txt")
    diagram = DiaChrom(ciexyz)
    clf()
    figure(3, figsize=(9, 8))
    xlabel('x')
    ylabel('y')
    diagram.plotSpectre()
    diagram.paintAll(adobeRGB, "Adobe RGB(98)", 0.001)
    diagram.paintAll(oneplus6t, "OnePlus 6T", 0.001, c='k', m='*')
    diagram.paintAll(oneplus6tManufacturer, "DCI-P3", 0.001, c='k', m='>')
    legend()
    axis([0, 1, 0, 1])
    grid(True)
    show()
