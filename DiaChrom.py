from matplotlib.pyplot import plot, imshow, text
from numpy import *
from math import *
from XYZ2RGB import *
from CieXYZ import *


class DiaChrom:
    def __init__(self, ciexyz):
        self.ciexyz = ciexyz
        self.kmin = 400 - 360
        self.kmax = 701 - 360
        self.ns = self.kmax - self.kmin
        spectre = ciexyz.xyColor()
        self.ymin = spectre[1][self.kmin]
        self.ymax = max(spectre[1])
        self.xSpectre = []
        self.ySpectre = []
        for k in range(self.ns):
            self.xSpectre.append(spectre[0][k + self.kmin])
            self.ySpectre.append(spectre[1][k + self.kmin])
        self.x1 = self.xSpectre[0]
        self.y1 = self.ySpectre[0]
        self.x2 = self.xSpectre[self.ns - 1]
        self.y2 = self.ySpectre[self.ns - 1]

    def plotSpectre(self):
        plot(self.xSpectre, self.ySpectre, c='k', label='_nolegend_')
        plot([self.x1, self.x2], [self.y1, self.y2], c='k', label='_nolegend_')
        for L in [400, 480, 500, 520, 550, 600, 700]:
            k = L - 400
            if L < 520:
                text(self.xSpectre[k], self.ySpectre[k], str(L), horizontalalignment='right')
            else:
                text(self.xSpectre[k], self.ySpectre[k], str(L), horizontalalignment='left')

    def paintRGB(self, xyz2rgb, name, delta):
        rgb = xyz2rgb
        plot([rgb.red[0], rgb.green[0], rgb.blue[0], rgb.red[0]], [rgb.red[1], rgb.green[1], rgb.blue[1], rgb.red[1]],
             color='k', marker='o', label=name)
        img = []
        largeur = rgb.red[0] - rgb.blue[0]
        hauteur = rgb.green[1] - rgb.blue[1]
        imax = int(floor(largeur / delta))
        jmax = int(floor(hauteur / delta))
        penteBG = (rgb.green[0] - rgb.blue[0]) / (rgb.green[1] - rgb.blue[1])
        penteBR = (rgb.red[0] - rgb.blue[0]) / (rgb.red[1] - rgb.blue[1])
        penteGR = (rgb.red[0] - rgb.green[0]) / (rgb.red[1] - rgb.green[1])
        for j in range(jmax):
            ligne = []
            y = rgb.blue[1] + float(j) * delta
            xmin = rgb.blue[0] + penteBG * (y - rgb.blue[1])
            if y < rgb.red[1]:
                xmax = rgb.blue[0] + penteBR * (y - rgb.blue[1])
            else:
                xmax = rgb.green[0] + penteGR * (y - rgb.green[1])
            for i in range(imax):
                x = rgb.blue[0] + float(i) * delta
                if (x >= xmin) & (x < xmax):
                    RGB = rgb.xyL2rgb(x, y, 1.0)
                    ligne.append([RGB[0], RGB[1], RGB[2], 1])
                else:
                    ligne.append([0, 0, 0, 0])
            img.append(ligne)
        imshow(img, origin='lower', extent=[rgb.blue[0], rgb.red[0], rgb.blue[1], rgb.green[1]])

    def paintAll(self, xyz2rgb, name, delta, c='k', m='o'):
        rgb = xyz2rgb
        img = []
        largeur = self.x2
        hauteur = self.ymax
        imax = int(floor(largeur / delta))
        jmax = int(floor(hauteur / delta))
        for j in range(jmax):
            ligne = []
            y = float(j) * delta
            k = 0
            while (y > self.ySpectre[k]):
                k += 1
            xmin = self.xSpectre[k]
            k += 1
            if y > self.y2:
                while (y < self.ySpectre[k]):
                    k += 1
                xmax = self.xSpectre[k]
            else:
                xmax = self.x1 + (y - self.y1) * (self.x2 - self.x1) / (self.y2 - self.y1)
            for i in range(imax):
                x = float(i) * delta
                if (x >= xmin) & (x < xmax):
                    RGB = rgb.xyL2rgb(x, y, 1.0)
                    ligne.append([RGB[0], RGB[1], RGB[2], 1])
                else:
                    ligne.append([0, 0, 0, 0])
            img.append(ligne)
        imshow(img, origin='lower', extent=[0, largeur, 0, hauteur])
        plot([rgb.red[0], rgb.green[0], rgb.blue[0], rgb.red[0]], [rgb.red[1], rgb.green[1], rgb.blue[1], rgb.red[1]],
             color=c, marker=m, label=name)

    def corpsNoir(self, Tarray):
        x = []
        y = []
        s = ""
        for T in Tarray:
            self.ciexyz.setIlluminant("CN", T)

            def Rf(L):
                return 1

            XYZ = self.ciexyz.spectralF2XYZ(Rf)
            xy = self.ciexyz.XYZ2xy(XYZ[0], XYZ[1], XYZ[2])
            x.append(xy[0])
            y.append(xy[1])
            s += "%.0f " % T
        plot(x, y, marker="+", color="k", linestyle="-", label="CN : %s K" % s)
