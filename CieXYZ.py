import re
from math import *


class CIEXYZ:
    def __init__(self, filename):
        f = open(filename)
        self.Lambda = []
        self.xc = []
        self.yc = []
        self.zc = []
        self.S = []
        self.Sxc = []
        self.Syc = []
        self.Szc = []
        I = 0
        for line in f.readlines():
            li = line.strip()
            t = re.split('[\s|,]+', li)
            self.Lambda.append(float(t[0]))
            self.xc.append(float(t[1]))
            self.yc.append(float(t[2]))
            self.zc.append(float(t[3]))
            self.S.append(1.0)
            self.Sxc.append(float(t[1]))
            self.Syc.append(float(t[2]))
            self.Szc.append(float(t[3]))
            I += float(t[2])
        self.K = 1.0 / I
        f.close()

    def fColor(self, L):
        if ((L < 360) | (L > 829)):
            return [0, 0, 0]
        else:
            i = int(floor(L - 360.0))
            x = self.xc[i] + (L - float(i) - 360.0) * (self.xc[i + 1] - self.xc[i])
            y = self.yc[i] + (L - float(i) - 360.0) * (self.yc[i + 1] - self.yc[i])
            z = self.zc[i] + (L - float(i) - 360.0) * (self.zc[i + 1] - self.zc[i])
            return [x, y, z]

    def XYZ2xy(self, X, Y, Z):
        return [X / (X + Y + Z), Y / (X + Y + Z)]

    def xyY2XYZ(self, x, y, Y):
        return [x / y * Y, Y, (1 - x - y) / y * Y]

    def xyColor(self):
        x = []
        y = []
        for k in range(len(self.xc)):
            xy = self.XYZ2xy(self.xc[k], self.yc[k], self.zc[k])
            x.append(xy[0])
            y.append(xy[1])
        return [x, y]

    def corpsNoir(self, L, T):
        return 100 * pow(560 / L, 5) * (exp(2.569e4 / T) - 1) / (exp(1.4388e7 / (L * T)) - 1)

    def readA(self, filename):
        f = open(filename)
        self.illumALambda = []
        self.illumASpectre = []
        for line in f.readlines():
            li = line.strip()
            t = re.split('[\s|,]+', li)
            self.illumALambda.append(float(t[0]))
            self.illumASpectre.append(float(t[1]))
        f.close()

    def readD65(self, filename):
        f = open(filename)
        self.illumD65Lambda = []
        self.illumD65Spectre = []
        for line in f.readlines():
            li = line.strip()
            t = re.split('[\s|,]+', li)
            self.illumD65Lambda.append(float(t[0]))
            self.illumD65Spectre.append(float(t[1]))
        f.close()

    def SpectreD65(self, L):
        L = floor(L)
        if ((L < 300) | (L > 830)):
            return 0
        else:
            i = int(L - 300)
            return self.illumD65Spectre[i]

    def setIlluminant(self, nom, T):
        if nom == "CN":
            for k in range(471):
                L = float(360 + k)
                self.S[k] = self.corpsNoir(L, T)
        elif nom == "D65":
            for k in range(471):
                self.S[k] = self.illumD65Spectre[60 + k]
        elif nom == "A":
            for k in range(471):
                L = float(360 + k)
                self.S[k] = self.corpsNoir(L, 2855.5)
        elif nom == 'E':
            for k in range(471):
                self.S[k] = 1
        I = 0
        for k in range(471):
            self.Sxc[k] = self.S[k] * self.xc[k]
            self.Syc[k] = self.S[k] * self.yc[k]
            self.Szc[k] = self.S[k] * self.zc[k]
            I += self.Syc[k]
        self.K = 1.0 / I

    def setFonctionIlluminant(self, f):
        for k in range(471):
            L = float(360 + k)
            self.S[k] = f(L)
        I = 0
        for k in range(471):
            self.Sxc[k] = self.S[k] * self.xc[k]
            self.Syc[k] = self.S[k] * self.yc[k]
            self.Szc[k] = self.S[k] * self.zc[k]
            I += self.Syc[k]
        self.K = 1.0 / I

    def spectralF2XYZ(self, Rf):
        X = 0
        Y = 0
        Z = 0
        for k in range(471):
            L = float(360 + k)
            R = Rf(L)
            X += self.Sxc[k] * R
            Y += self.Syc[k] * R
            Z += self.Szc[k] * R
        return [self.K * X, self.K * Y, self.K * Z]

    def XYZMonochrome(self, L):
        if (L < 360) | (L > 830):
            return [0, 0, 0]
        k = int(floor(L - 360))
        X = self.Sxc[k]
        Y = self.Syc[k]
        Z = self.Szc[k]
        return [self.K * X, self.K * Y, self.K * Z]

    def ener2lum(self, e, L):
        xyz = self.fColor(L)
        return e * 683.0 * xyz[1]

    def lum2ener(self, lu, L):
        xyz = self.fColor(L)
        return lu / (683.0 * xyz[1])

    def rgbColorim(self):
        r = []
        g = []
        b = []
        for k in range(len(self.xc)):
            r.append(0.41846 * self.xc[k] - 0.15866 * self.yc[k] - 0.08283 * self.zc[k])
            g.append(-0.09117 * self.xc[k] + 0.25242 * self.yc[k] + 0.01571 * self.zc[k])
            b.append(0.00092 * self.xc[k] - 0.00255 * self.yc[k] + 0.17860 * self.zc[k])
        return [r, g, b]
